package wang.jingzhao.command_help;

import android.app.*;
import android.content.*;
import android.os.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import android.widget.AdapterView.*;
import java.util.*;
import wang.jingzhao.command_help.Util.*;

public class CommandActivity extends BaseActivity
{
	private List<Command> CommandList = new ArrayList<Command>();

	public static Activity getActivity = null;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		Log.i(theApplication.LOGTAG,"已调用菜单");
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.commandxml);
		getActivity = this;
		initCommand();
		initActivity();
		initButtonClick();
		CommandAdapter adapter = new CommandAdapter(this,R.layout.command_item,CommandList);
		ListView listview = (ListView) findViewById(R.id.commandxmlListView1);
		listview.setAdapter(adapter);
		listview.setOnItemClickListener(new OnItemClickListener()
			{

				@Override
				public void onItemClick(AdapterView<?> p1, View p2, int p3, long p4)
				{
					Intent intent = new Intent(theApplication.getapplication(),UseActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
					theApplication.CommandNumber = (int) p4;
				}

			});
		
	}
	public void initCommand()
	{
		/*//测试代码
		 CommandList.add(new Command("/gamemode","更改游戏模式"));
		 //测试代码*/
		/*String allcommandlist[] = getResources().getStringArray(R.array.allcommandlist);
		 for(int i = 0;i < allcommandlist.length;i++)
		 {
		 String s = allcommandlist[i];
		 Log.i(theApplication.LOGTAG,allcommandlist[i]);
		 Resources res = getResources();    
		 int titleid=res.getIdentifier(s,"array","wang.jingzhao.command_help");
		 Log.i(theApplication.LOGTAG,"" + titleid);
		 String acommand[] = res.getStringArray(titleid);
		 Log.i(theApplication.LOGTAG,acommand[0]+" and "+acommand[1]);
		 CommandList.add(new Command(acommand[0],acommand[1]));
		 //CommandList.add(
		 }*/
		
		for (int m = 0;m < theApplication.CommandListData.length;m++)
		{
			CommandList.add(new Command(theApplication.CommandListData[m][0],theApplication.CommandListData[m][1]));
		}
	}
	public void initActivity()
	{
		Display display = getWindowManager().getDefaultDisplay(); // 为获取屏幕宽、高
		Window window = getWindow();
		LayoutParams windowLayoutParams = window.getAttributes(); // 获取对话框当前的参数值
		windowLayoutParams.width = (int) (display.getWidth() * 0.75); // 宽度设置为屏幕的倍数
		windowLayoutParams.height = (int) (display.getHeight() * 0.75); // 高度设置为屏幕的倍数
	}
	public void initButtonClick()
	{
		ImageView ButtonBack = (ImageView) findViewById(R.id.commandxmlBack);
		ImageView ButtonClose = (ImageView) findViewById(R.id.commandxmlClose);
		Button ButtonFinish = (Button) findViewById(R.id.commandxmlFinishButton);
		OnClickListener onClickCloseListener = new OnClickBackAndClose();
		OnClickListener onClickFinish = new OnClickFinish();
		ButtonBack.setOnClickListener(onClickCloseListener);
		ButtonClose.setOnClickListener(onClickCloseListener);
		ButtonFinish.setOnClickListener(onClickFinish);
	}
	public static Activity getActivity()
	{
		return getActivity;
	}
}
class OnClickBackAndClose implements OnClickListener
{

	@Override
	public void onClick(View p1)
	{
		ActivityManagerApplication.addDestoryActivity(CommandActivity.getActivity(),"CommandActivity");
		ActivityManagerApplication.destoryActivity("CommandActivity");
	}

}
class OnClickFinish implements OnClickListener
{

	@Override
	public void onClick(View p1)
	{
		Intent intent = new Intent(theApplication.getapplication(),WindowService.class);
		WindowService.finishme();
		theApplication.getapplication().stopService(intent);
		Toast.makeText(theApplication.getapplication(),"悬浮窗关闭需要0-5秒" + "\n" + "请耐心等待！",Toast.LENGTH_LONG).show();
		
		RemoveActivityManager ram = new RemoveActivityManager();
		ram.removeALLActivity();
		
		Toast.makeText(theApplication.getapplication(),"正在强制退出软件" + "\n" + "部分手机可能提示崩溃，请忽略！",Toast.LENGTH_LONG).show();
		System.exit(0);
		
	}

}
