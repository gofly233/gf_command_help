package wang.jingzhao.command_help;

import android.app.*;
import android.os.*;
import android.view.*;
import android.widget.AdapterView.*;
import android.widget.*;
import android.text.method.*;
import android.graphics.*;
import android.text.*;
import wang.jingzhao.command_help.Util.*;

public class UseActivity extends BaseActivity
{
	public static Activity getActivity = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		getActivity = this;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.useactivity);
		initActivity();
		initButtonClick();
		initLayout();
	}
	public void initActivity()
	{
		Display display = getWindowManager().getDefaultDisplay(); // 为获取屏幕宽、高
		Window window = getWindow();
		LayoutParams windowLayoutParams = window.getAttributes(); // 获取对话框当前的参数值
		windowLayoutParams.width = (int) (display.getWidth() * 0.75); // 宽度设置为屏幕的倍数
		windowLayoutParams.height = (int) (display.getHeight() * 0.75); // 高度设置为屏幕的倍数
	}
	public void initButtonClick()
	{
		ImageView ButtonBack = (ImageView) findViewById(R.id.useactivityBack);
		ImageView ButtonClose = (ImageView) findViewById(R.id.useactivityClose);
		OnClickListener onClickListenerBack = new OnClickBack();
		OnClickListener onClickListenerClose = new OnClickClose();
		ButtonBack.setOnClickListener(onClickListenerBack);
		ButtonClose.setOnClickListener(onClickListenerClose);
	}
	public void initLayout()
	{
		int CommandNumber = theApplication.CommandNumber;
		TextView CommandName = (TextView) findViewById(R.id.useactivityTextViewName);
		TextView CommandDes = (TextView) findViewById(R.id.useactivityTextViewDes);
		TextView CommandUse = (TextView) findViewById(R.id.useactivityTextViewUse);
		TextView Commandtips = (TextView) findViewById(R.id.useactivityTextViewtips);
		CommandName.setText(Html.fromHtml(theApplication.CommandListData[CommandNumber][0]));
		CommandDes.setText(Html.fromHtml(theApplication.CommandListData[CommandNumber][1]));
		CommandUse.setText(Html.fromHtml(theApplication.CommandListData[CommandNumber][2]));
		
		CommandName.setTypeface(Typeface.createFromAsset(theApplication.getapplication().getAssets(),"quote/MC_And_FZXS12.ttf"));
		CommandDes.setTypeface(Typeface.createFromAsset(theApplication.getapplication().getAssets(),"quote/MC_And_FZXS12.ttf"));
		CommandUse.setTypeface(Typeface.createFromAsset(theApplication.getapplication().getAssets(),"quote/MC_And_FZXS12.ttf"));
		Commandtips.setTypeface(Typeface.createFromAsset(theApplication.getapplication().getAssets(),"quote/MC_And_FZXS12.ttf"));
		
		try
		{
			ShortCodeToHTML scth = new ShortCodeToHTML(FindByAssets.InputStreamTOString((getAssets().open("quote/tips.txt"))));
			Commandtips.setText(Html.fromHtml(scth.getHTML()));
		}
		catch (Exception e)
		{}
		//CommandUse.setMovementMethod(ScrollingMovementMethod.getInstance());
	}
	public static Activity getActivity()
	{
		return getActivity;
	}
}
class OnClickBack implements OnClickListener
{

	@Override
	public void onClick(View p1)
	{
		ActivityManagerApplication.addDestoryActivity(UseActivity.getActivity(),"UseActivity");
		ActivityManagerApplication.destoryActivity("UseActivity");
	}

	
	

}
class OnClickClose implements OnClickListener
{

	@Override
	public void onClick(View p1)
	{
		ActivityManagerApplication.addDestoryActivity(UseActivity.getActivity(),"UseActivity");
		ActivityManagerApplication.destoryActivity("UseActivity");
		
		ActivityManagerApplication.addDestoryActivity(CommandActivity.getActivity(),"CommandActivity");
		ActivityManagerApplication.destoryActivity("CommandActivity");
	}

	
}
