package wang.jingzhao.command_help;

import android.app.*;
import com.tencent.bugly.*;
import android.content.*;
import com.tencent.bugly.beta.*;
import java.util.*;

public class theApplication extends Application
{

	public static String LOGTAG = "GFCH";

	public static theApplication appcon = null;

	public static String[][] CommandListData;

	public static int CommandNumber;

	//public static Intent WindowsServiceIntent = new Intent(theApplication.getapplication(),WindowService.class);

	@Override
	public void onCreate()
	{
		super.onCreate();
		appcon = (theApplication) getApplicationContext();
		//初始化腾讯Bugly
		//保密需要已屏蔽APPID
		Context appcontext = getApplicationContext();
		String APPID = appcontext.getResources().getString(R.string.APPID);
		Bugly.init(appcontext,APPID,false);
		//自动检测
		Beta.autoCheckUpgrade = true;
		//初始化延迟
		Beta.initDelay = 1 * 1000;
		//设置通知栏图标
		Beta.largeIconId = R.drawable.logo;
		Beta.smallIconId = R.drawable.logo;
		//禁用热更新
		Beta.enableHotfix = false;
	}

	public static theApplication getapplication()
	{
		return appcon;
	}

}
