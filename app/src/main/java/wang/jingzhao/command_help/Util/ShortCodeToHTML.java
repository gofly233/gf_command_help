package wang.jingzhao.command_help.Util;

import wang.jingzhao.command_help.*;

public class ShortCodeToHTML
{
	private String shortCode;

	public String [] colorArray = {"white","ivory","lightyellow","yellow","snow","floralwhite","lemonchiffon","cornsilk","seashell","lavenderblush","papayawhip","blanchedalmond","mistyrose","bisque","moccasin","navajowhite","peachpuff","gold","pink","lightpink","orange","lightsalmon","darkorange","coral","hotpink","tomato","orangered","deeppink","fuchsia","magenta","red","oldlace","lightgoldenrodyellow","linen","antiquewhite","salmon","ghostwhite","mintcream","whitesmoke","beige","wheat","sandybrown","azure","honeydew","aliceblue","khaki","lightcoral","palegoldenrod","violet","darksalmon","lavender","lightcyan","burlywood","plum","gainsboro","crimson","palevioletred","goldenrod","orchid","thistle","lightgray","lightgrey","tan","chocolate","peru","indianred","mediumvioletred","silver","darkkhaki","rosybrown","mediumorchid","darkgoldenrod","firebrick","powderblue","lightsteelblue","paleturquoise","greenyellow","lightblue","darkgray","brown","sienna","darkorchid","palegreen","darkviolet","mediumpurple","lightgreen","darkseagreen","saddlebrown","darkmagenta","darkred","blueviolet","lightskyblue","skyblue","gray","grey","olive","purple","maroon","aquamarine","chartreuse","lawngreen","mediumslateblue","lightslategray","lightslategrey","slategray","slategrey","olivedrab","slateblue","dimgray","dimgrey","mediumaquamarine","cornflowerblue","cadetblue","darkolivegreen","indigo","mediumturquoise","darkslateblue","steelblue","royalblue","turquoise","mediumseagreen","limegreen","darkslategray","darkslategrey","seagreen","forestgreen","lightseagreen","dodgerblue","midnightblue","aqua","cyan","springgreen","lime","mediumspringgreen","darkturquoise","deepskyblue","darkcyan","teal","green","darkgreen","blue","mediumblue","darkblue","navy","black"};

	public ShortCodeToHTML(String shortCode)
	{
		this.shortCode = shortCode;
	}
	
	public void setShortCode(String shortCode)
	{
		this.shortCode = shortCode;
	}
	
	public String getHTML()
	{
		/*
		 关键部分伪代码

		 if(字符串含有"<")
		 {
		 if("<"前面没有\)
		 {
		 搜索<后面匹配的>位置
		 再搜索后面的</>
		 把里面的换成HTML代码
		 }
		 }
		 */
		String shortCode = this.shortCode;

		String HTMLCode;

		//String colorTitle = null;

		int resourceNumber;
		for (int i = 0;i < colorArray.length;i++)
		{
			resourceNumber = theApplication.getapplication().getResources().getIdentifier(colorArray[i], "color", "wang.jingzhao.command_help");
			int color = theApplication.getapplication().getResources().getColor(resourceNumber);
			shortCode = shortCode.replace("<" + colorArray[i] + ">", "<font color=\"" + "#" + encodeHex(color) + "\">");//</font>
			shortCode = shortCode.replace("</>", "</font>");
			shortCode = shortCode.replace("\n","<br>");
			shortCode = shortCode.replace("\\n","<br>");
		}
		
		HTMLCode = shortCode;
		return HTMLCode;
	}
	
	private String encodeHex(int integer)
	{ 
		StringBuffer buf = new StringBuffer(6); 
		if (((int) integer & 0xffffff) < 0x10)
		{ 
			buf.append("0"); 
		} 
		buf.append(Long.toString((int) integer & 0xffffff, 16)); 
		return buf.toString(); 
	}

}
