package wang.jingzhao.command_help.Util;

import wang.jingzhao.command_help.*;
import java.util.*;
import android.app.*;

public class RemoveActivityManager
{

	private static List<Activity> oList = new ArrayList<>();

	/**
	 * 添加Activity
	 */
	public void addActivity(Activity activity)
	{
		if (!(activity == null))
		{
			// 判断当前集合中不存在该Activity
			if (!oList.contains(activity))
			{
				oList.add(activity);//把当前Activity添加到集合中
			}
		}
	}

	/**
	 * 销毁单个Activity
	 */
	public  void removeActivity(Activity activity)
	{
		//判断当前集合中存在该Activity
		if (oList.contains(activity))
		{
			oList.remove(activity);//从集合中移除
			activity.finish();//销毁当前Activity
		}
	}

	/**
	 * 销毁所有的Activity
	 */
	public void removeALLActivity()
	{
		//通过循环，把集合中的所有Activity销毁
		for (Activity activity : oList)
		{
			activity.finish();
		}
	}
}
