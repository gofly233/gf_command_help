package wang.jingzhao.command_help.Util;

import wang.jingzhao.command_help.*;
import java.util.*;
import android.app.*;

public class RemoveServiceManager
{

	private static List<Service> oList = new ArrayList<>();

	/**
	 * 添加Service
	 */
	public void addService(Service service)
	{
		if (!(service == null))
		{
			// 判断当前集合中不存在该Service
			if (!oList.contains(service))
			{
				oList.add(service);//把当前Service添加到集合中
			}
		}
	}

	/**
	 * 销毁单个Service
	 */
	public  void removeService(Service service)
	{
		//判断当前集合中存在该Service
		if (oList.contains(service))
		{
			oList.remove(service);//从集合中移除
			service.stopSelf();//销毁当前Service
		}
	}

	/**
	 * 销毁所有的Service
	 */
	public void removeALLService()
	{
		//通过循环，把集合中的所有Service销毁
		for (Service service : oList)
		{
			service.stopSelf();
		}
	}
}
