package wang.jingzhao.command_help;

import android.widget.*;
import android.content.*;
import java.util.*;
import android.view.*;
import android.graphics.*;
import android.text.*;

public class CommandAdapter extends ArrayAdapter
{
	private final int resourceId;

    public CommandAdapter(Context context, int textViewResourceId, List<Command> objects) {
        super(context, textViewResourceId, objects);
        resourceId = textViewResourceId;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Command command = (Command) getItem(position); // 获取当前项的Command实例
        View view = LayoutInflater.from(getContext()).inflate(resourceId, null);//实例化一个对象
        TextView Commandname = (TextView) view.findViewById(R.id.commanditemTextView1);//获取该布局内的便条标题
        TextView Commanddescribe = (TextView) view.findViewById(R.id.commanditemTextView2);//获取该布局内的便条简介
        Commandname.setText(Html.fromHtml(command.getName()));//为标题设置文本内容
        Commanddescribe.setText(Html.fromHtml(command.getDescribe()));//为简介设置文本内容
		Commandname.setTypeface(Typeface.createFromAsset(theApplication.getapplication().getAssets(),"quote/MC_And_FZXS12.ttf"));
		Commanddescribe.setTypeface(Typeface.createFromAsset(theApplication.getapplication().getAssets(),"quote/MC_And_FZXS12.ttf"));
        return view;
    }
}

