package wang.jingzhao.command_help;

import android.app.*;
import android.os.*;
import android.view.*;
import android.view.ViewGroup.*;
import wang.jingzhao.command_help.Util.*;
import android.support.v7.app.*;

public abstract class BaseActivity extends Activity
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		addActivity(this);
	}
	
	protected void addActivity(BaseActivity activity)
	{
		RemoveActivityManager ram = new RemoveActivityManager();
		ram.addActivity(activity);
	}
}
