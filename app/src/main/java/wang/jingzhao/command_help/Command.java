package wang.jingzhao.command_help;

public class Command
{
	private String name;
	private String describe;
	
	public Command(String name, String describe) {
		this.name = name;
		this.describe = describe;
	}

	public String getName() {
		return name;
	}

	public String getDescribe() {
		return describe;
	}
	
}
