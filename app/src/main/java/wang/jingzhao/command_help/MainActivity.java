package wang.jingzhao.command_help;

import android.app.*;
import android.os.*;
import android.view.*;
import android.view.View.*;
import android.widget.*;
import android.provider.*;
import android.content.*;
import android.util.*;
import com.tencent.bugly.beta.*;
import wang.jingzhao.command_help.Util.*;

public class MainActivity extends BaseActivity 
{
	//private android.support.v7.widget.Toolbar toolbar;
	
	//定义按钮监听器
	public OnClickListener OnClickRun = new OnClickListener()
	{

		@Override
		public void onClick(View button)
		{
			//重写onClick
			//调用MainActivity.Run()
			Run();
		}

	};

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
		//设置界面
        setContentView(R.layout.main);
		//设置按钮监听器
		Button button = (Button) findViewById(R.id.RunButton);
		button.setOnClickListener(OnClickRun);
		Log.i(theApplication.LOGTAG, "已启动");
		//调用腾讯Bugly检测更新
		//第一个参数 自动检测为false,用户点击为true
		//第二个参数 不带弹窗与Toast为false,反之则为true
		//Beta.checkUpgrade(false,false);
		
		//toolbar = (android.support.v7.widget.Toolbar) findViewById((R.id.toolbar));
		//setSupportActionBar(toolbar);
    }

	public void CheckUpdate(View view)
	{
		//调用腾讯Bugly检测更新
		//第一个参数 自动检测为false,用户点击为true
		//第二个参数 不带弹窗与Toast为false,反之则为true
		Beta.checkUpgrade(true, false);
	}

	public void Run()
	{
		Boolean candraw = CanDraw();
		if (candraw)
		{
			//启动代码
			Toast.makeText(this, "正在初始化悬浮窗..." + "\n" + "请耐心等待......", Toast.LENGTH_LONG).show();
			Intent intent = new Intent(theApplication.getapplication(), WindowService.class);
			startService(intent);
			finish();
		}
		else
		{
			ToCanDraw();
		}
	}

	public Boolean CanDraw()
	{
		if (Build.VERSION.SDK_INT >= 23)
		{
			if (Settings.canDrawOverlays(this))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return true;
		}
	}

	public void ToCanDraw()
	{
		Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
		Toast.makeText(this, "请点击允许以开启悬浮窗", Toast.LENGTH_LONG).show();
		startActivity(intent);
	}
}
