package wang.jingzhao.command_help;

import java.io.*;
import android.util.*;
import org.json.*;
import java.util.*;
import wang.jingzhao.command_help.Util.*;

public class FindByAssets
{
	final static int BUFFER_SIZE = 4096;

	public static String[][] ReturnData()
	{
		String[] AssetsCommandFileList;
		String[] CommandText;
		String[][] returndata;

		try
		{
			AssetsCommandFileList = theApplication.getapplication().getResources().getAssets().list("command");
			Arrays.sort(AssetsCommandFileList);
			for (int i = 0;i < AssetsCommandFileList.length;i++)
			{
				Log.i(theApplication.LOGTAG, "已找到文件:" + AssetsCommandFileList[i]);
			}
			CommandText = new String[AssetsCommandFileList.length];
			for (int i = 0;i < AssetsCommandFileList.length;i++)
			{
				try
				{
					CommandText[i] = InputStreamTOString(theApplication.getapplication().getAssets().open("command/" + AssetsCommandFileList[i]));
				}
				catch (Exception e)
				{}
				Log.i(theApplication.LOGTAG, AssetsCommandFileList[i] + "文件的内容为:" + CommandText[i]);
			}
			JSONObject jsonobject = null;
			returndata = new String[AssetsCommandFileList.length][3];
			for (int i = 0;i < AssetsCommandFileList.length;i++)
			{

				String[] returndatatemp = new String[3];

				try
				{
					jsonobject = new JSONObject(CommandText[i]);
				}
				catch (JSONException e)
				{}
				//Log.i(theApplication.LOGTAG,jsonobject.getString("name"));
				try
				{
					returndatatemp[0] = jsonobject.getString("name");
				}
				catch (JSONException e)
				{returndatatemp[0] = "error";}
				try
				{
					returndatatemp[1] = jsonobject.getString("des");
				}
				catch (JSONException e)
				{returndatatemp[1] = "error";}
				try
				{
					returndatatemp[2] = jsonobject.getString("use");
				}
				catch (JSONException e)
				{returndatatemp[2] = "暂无数据";}
				
				for (int a = 0;a < returndatatemp.length;a++)
				{
					ShortCodeToHTML scth = new ShortCodeToHTML(returndatatemp[a]);
					returndatatemp[a] = scth.getHTML();
				}
				returndata[i] = returndatatemp;
				Log.i(theApplication.LOGTAG, returndata[i][0] + " " + returndata[i][1] + " " + returndata[i][2]);
				//returndata[i][2]

			}
		}
		catch (IOException e)
		{
			AssetsCommandFileList = new String[0];
			return null;
		}
		for (int i = 0;i < AssetsCommandFileList.length;i++)
		{

		}
		return returndata;
	}

	public static String InputStreamTOString(InputStream in) throws Exception
	{

        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        byte[] data = new byte[BUFFER_SIZE];
        int count = -1;
        while ((count = in.read(data, 0, BUFFER_SIZE)) != -1)
            outStream.write(data, 0, count);

        data = null;
		//"ISO-8859-1"
        return new String(outStream.toByteArray(), "UTF-8");
    }
}
