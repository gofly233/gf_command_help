package wang.jingzhao.command_help;

import android.app.*;
import android.content.*;
import android.os.*;
import android.view.*;
import android.widget.TableRow.*;
import android.widget.*;
import android.graphics.*;
import android.util.*;
import android.widget.AdapterView.*;
import wang.jingzhao.command_help.control.*;
import wang.jingzhao.command_help.Util.*;

public class WindowService extends Service
{
	public static Service thisService;
	
	//Log用的TAG
	private static final String TAG = theApplication.LOGTAG;
	//要引用的布局文件.
    LinearLayout toucherLayout;
    //布局参数.
    WindowManager.LayoutParams params;
    //实例化的WindowManager.
    WindowManager windowManager;
	//状态栏高度
	int statusBarHeight = -1;
	@Override
	public IBinder onBind(Intent intent)
	{
		return null;
	}

	@Override
	public void onCreate()
	{
		super.onCreate();
		Log.i(TAG,"服务已启动");
		thisService = this;
		//添加销毁List
		RemoveServiceManager rsm = new RemoveServiceManager();
		rsm.addService(this);
		//生成悬浮窗
		createToucher();
	}

	private void createToucher()
	{
		//赋值WindowManager&LayoutParam.
        params = new WindowManager.LayoutParams();
        windowManager = (WindowManager) getApplication().getSystemService(Context.WINDOW_SERVICE);
        //设置type.系统提示型窗口，一般都在应用程序窗口之上.
        params.type = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;
        //设置效果为背景透明.
        params.format = PixelFormat.RGBA_8888;
        //设置flags.不可聚焦及不可使用按钮对悬浮窗进行操控.
        params.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;

        //设置窗口初始停靠位置.
        params.gravity = Gravity.LEFT | Gravity.TOP;
        params.x = 0;
        params.y = 0;

        //设置悬浮窗口长宽数据.
        //注意，这里的width和height均使用px而非dp.这里我偷了个懒
        //如果你想完全对应布局设置，需要先获取到机器的dpi
        //px与dp的换算为px = dp * (dpi / 160).
		//调用了换算类
		int dip = DensityUtil.dip2px(getApplicationContext(),40);
        params.width = dip;
        params.height = dip;

        LayoutInflater inflater = LayoutInflater.from(getApplication());
        //获取浮动窗口视图所在布局.
        toucherLayout = (LinearLayout) inflater.inflate(R.layout.gfbutton,null);
		//初始化指令列表
		initCommandList();
        //添加toucherlayout
        windowManager.addView(toucherLayout,params);

        Log.i(TAG,"toucherlayout-->left:" + toucherLayout.getLeft());
        Log.i(TAG,"toucherlayout-->right:" + toucherLayout.getRight());
        Log.i(TAG,"toucherlayout-->top:" + toucherLayout.getTop());
        Log.i(TAG,"toucherlayout-->bottom:" + toucherLayout.getBottom());

        //主动计算出当前View的宽高信息.
        toucherLayout.measure(View.MeasureSpec.UNSPECIFIED,View.MeasureSpec.UNSPECIFIED);

        //用于检测状态栏高度.
        int resourceId = getResources().getIdentifier("status_bar_height","dimen","android");
        if (resourceId > 0)
        {
            statusBarHeight = getResources().getDimensionPixelSize(resourceId);
        }
		//定义RoundImageView
		RoundImageView roundimageview = (RoundImageView) toucherLayout.findViewById(R.id.gfbuttonRoundImageView1);
		roundimageview.setOnClickListener(new View.OnClickListener()
			{

				@Override
				public void onClick(View view)
				{
					//Toast.makeText(getApplication(),"开发中ing...",Toast.LENGTH_SHORT).show();
					Intent intent = new Intent(getApplication(),CommandActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					getApplicationContext().startActivity(intent);
				}


			});
		roundimageview.setOnTouchListener(new View.OnTouchListener()
			{
				@Override
				public boolean onTouch(View v, MotionEvent event)
				{
					int dix = DensityUtil.dip2px(getApplicationContext(),40);
					//ImageButton我放在了布局中心，布局一共75dp
					params.x = (int) event.getRawX() - (dix / 2);
					//这就是状态栏偏移量用的地方
					params.y = (int) event.getRawY() - (dix / 2) - statusBarHeight;
					windowManager.updateViewLayout(toucherLayout,params);
					return false;

				}
			});


	}
	/*public void showCXBRunning() {
	 NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
	 Notification builder = new Notification.Builder(this).setTicker("").setSmallIcon(R.drawable.logo).build();
	 startForeground(1, builder);
	 }*/
	public void initCommandList()
	{
		FindByAssets findbyassets = new FindByAssets();
		theApplication.CommandListData = findbyassets.ReturnData();
	}
	public static void finishme()
	{
		thisService.stopSelf();
	}
}
